export const PrepareSass = class {

  variables: object = {}
  resetCSS = true
  clearfix = true
  useDefaultFont = false
  sassVariablesPrefix = ''
  
  defaultFont = ''
  variablesText = ''
  resetCSSText = `

*{
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box;
}
`
  clearfixText: string = `
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
`
  defaultFontText = ''

  constructor (values: object = {}) {
    Object.assign(this, values)

    this.useDefaultFont = this.defaultFont.length > 0

    if (this.useDefaultFont) {
      this.defaultFontText = `
@import url('https://fonts.googleapis.com/css?family=${this.defaultFont}:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i');
*{
  font-family: '${this.defaultFont}', sans-serif;
}
`
    }

    if (!this.resetCSS) this.resetCSSText = ''
    if (!this.clearfix) this.clearfixText = ''

  }

  private setVariablesText(): void {

    const traverse = (data: any, name: string = ''): void => {

      const prefix = name != '' ? name + '_' : this.sassVariablesPrefix
    
      Object.entries(data).forEach(([key, value]) => {
    
        if(typeof value !== 'object') {
          this.variablesText += `
$${prefix}${key}: ${value};`
          return
        }

        traverse(data[key], prefix + key)
      })
      
    }
    
    traverse(this.variables)
  }

  compileToSass(): string {
    this.setVariablesText()
    return this.defaultFontText + this.variablesText + this.resetCSSText + this.clearfixText
  }
  
}