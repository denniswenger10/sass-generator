import * as util from 'util'
import { PrepareSass } from './prepareSass'
import { generateFiles } from './generateFiles'

export = class SassGenerator<T> {

  path = './'
  fileName = 'globalStyles'
  variables: object = {}
  sassVariablesPrefix = ''
  resetCSS = true
  clearfix = true
  defaultFont = ''
  private prepareSass: any

  constructor (values: object = {}) {
    Object.assign(this, values)

    this.prepareSass = new PrepareSass(this)
  }

  private jsText(): string {
    return `export default ${util.inspect(this.variables, false, 2, false)}`
  }

  async updateFiles() {
    await generateFiles(this.path, this.fileName, this.prepareSass.compileToSass(), this.jsText())
    console.log('Sass generator is done.')
  }

}