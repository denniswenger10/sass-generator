import { makeDirRecurse } from './makeDirRecurse'
import * as fs from 'fs'

export const generateFiles = (path: string, fileName: string, sassText: string, jsText: string) => {

  return new Promise(async resolve => {

    if (!fs.existsSync(path)) await makeDirRecurse(path)

    const js = async () => {
      await fs.promises.writeFile(`${path}/${fileName}.js`, jsText)
      console.log('Your JavaScript file has been created.')
    }

    const sass = async () => {
      await fs.promises.writeFile(`${path}/${fileName}.scss`, sassText)
      console.log('Your sass-file has been created.')
    }

    await Promise.all([ js(), sass() ])
    
    resolve()

  })

}
