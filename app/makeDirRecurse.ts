import * as fs from 'fs'
import * as path from 'path'

export const makeDirRecurse = (dirToMake: string) => {

  return new Promise( async (resolve, reject) => {

    const noneExistingFolders: string[] = []

    const testCurrentPath = (currentPath: string) => {

      const folder: string = path.normalize(currentPath)

      if (!fs.existsSync(folder)) {
        noneExistingFolders.unshift(currentPath)
        testCurrentPath(path.dirname(currentPath))
      } 
    }

    testCurrentPath(dirToMake)

    await noneExistingFolders.forEach(folder => fs.mkdirSync(folder))
    resolve()
  })

}
