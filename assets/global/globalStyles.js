export default {
  colors: {
    black: 'rgb(45,45,45)',
    gray: {
      light: 'rgb(210, 210, 210)',
      mid: 'rgb(175, 175, 175)',
      dark: 'rgb(85, 85, 85)'
    },
    blue: {
      light: 'rgb(90,140,255)',
      mid: 'rgb(30, 103, 240)',
      dark: 'rgb(10,30,205)'
    },
    red: { mid: 'rgb(199, 61, 37)' },
    green: { mid: 'rgb(32, 160, 53)' }
  },
  fontSize: { big: '24px', small: '16px' }
}