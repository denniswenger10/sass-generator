# Sass Generator
A developer build tool to generate common variables for Sass and JavaScript. Helping you to share the same styling across your project. 

## Table of Contents
  - [General info](#general-info)
  - [Technologies](#technologies)
  - [Status](#status)
  - [Setup](#setup)
  - [Contact](#contact)

## General info
As my projects have scaled more I've found it more important to set up good global styling variables in a Sass file to keep the my styling consistent and also easily managed. 

Unfortunately there are times when you need to style components with JavaScript. This is where this little project comes in. You get a config file where you set up your variables and a function that will generate a JS file and a Sass file with the same variables.

Consistent styling and one place to manage it <3

## Technologies
Project is created with:
* TypeScript
* Node
* Parcel

## Status
Under development.

## Setup
How to install:
```bash
$ cd *your project folder*
$ npm i @wentools/sass-generator
```
Use the following code in preferred build tool:
```javascript
const SassGenerator = require('@wentools/sass-generator')
sassGenerator = new SassGenerator()
sassGenerator.updateFiles()
```
The code above would generate an empty Javascript file in this directory and a Sass file that looks like this:
```scss
*{
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box;
}

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
```
That is not super useful. You do get a CSS reset and a clearfix, which is nice, but we want more. 

You may want to add some variables. So you try this:
```javascript
const SassGenerator = require('@wentools/sass-generator')
// import SassGenerator from '@wentools/sass-generator'

sassGenerator = new SassGenerator({
  variables = {
    colors: {
      blue: {
        light: 'rgb(90,140,255)',
        dark: 'rgb(10,30,205)',
      }
    }
    fontSize: {
      big: '24px',
      small: '16px'
    }
  }
})

sassGenerator.updateFiles()
```
Now the same variables and structure will be written to your JavaScript file and your Sass file will look like this:
```scss
$red: rgb(255,0,0);
$blue_light: rgb(90,140,255);
$blue_dark: rgb(10,30,205);

*{
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box;
}

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
```
So as you can see you get the same structured variables here as you have available in you JavaScript, with the difference that you have an underscore to represent deeper properties instead of dots, as in JS.

Another thing you have at your disposal is automatic imports of Google Fonts (this may expand to more resources later). 

if you would change your code to this:
```javascript
const SassGenerator = require('@wentools/sass-generator')

const sassGenerator = new SassGenerator({
  defaultFont: 'Quicksand'
})

sassGenerator.updateFiles()
```
Your Sass file import and automatically set your default font like this:
```scss
*{
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box;
}

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}

@import url('https://fonts.googleapis.com/css?family=Quicksand::100,100i,300,300i,400,400i,500,500i,700,700i,900,900i');
*{
  font-family: 'Quicksand', sans-serif;
}
```
Because of CSS specificity you can easily override this by just setting another font on a class or id. 

Now it is up to you on how to use these files. Being a fan of Vue I've personally mostly included the Sass file as a global asset in the vue.config.js to have access to the variables in all of my components and then imported the JS file in components when I've needed the variables.

A gotcha, the generated JS file is imported in the ES6 way:
```javascript
import *chosen filename* from *chosen directory + filename*
```

## Contact
Created by Dennis Wenger.
Hit me up on: denniswenger10@gmail.com